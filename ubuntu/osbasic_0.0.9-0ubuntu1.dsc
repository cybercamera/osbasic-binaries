Format: 1.0
Source: osbasic
Binary: osbasic
Architecture: all
Version: 0.0.9-0ubuntu1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9), gambas3-dev (>= 3.16), gambas3-dev (<< 3.99.0), gambas3-gb-args (>= 3.16), gambas3-gb-args (<< 3.99.0), gambas3-gb-image (>= 3.16), gambas3-gb-image (<< 3.99.0), gambas3-gb-form (>= 3.16), gambas3-gb-form (<< 3.99.0), gambas3-gb-eval-highlight (>= 3.16), gambas3-gb-eval-highlight (<< 3.99.0), gambas3-gb-util (>= 3.16), gambas3-gb-util (<< 3.99.0), gambas3-gb-form-editor (>= 3.16), gambas3-gb-form-editor (<< 3.99.0), gambas3-gb-term (>= 3.16), gambas3-gb-term (<< 3.99.0), gambas3-gb-form-terminal (>= 3.16), gambas3-gb-form-terminal (<< 3.99.0), gambas3-gb-scripter
Package-List:
 osbasic deb contrib/editors optional arch=all
Checksums-Sha1:
 cd65441e67ae854e755d300b63a2bc2751a2a2dc 1737426 osbasic_0.0.9.orig.tar.gz
 9c9d77f9ceb400758814ede2a21e1ff7005aa7ca 1712 osbasic_0.0.9-0ubuntu1.diff.gz
Checksums-Sha256:
 18fe3022518872dac81f1c0fd2828d433d3166f63d3bd1e8752233d74c418320 1737426 osbasic_0.0.9.orig.tar.gz
 94d0474b575c9226cf51bfdb5e2290bab7452d4d277979b52f1eea667ad0dcae 1712 osbasic_0.0.9-0ubuntu1.diff.gz
Files:
 2189d5a00265398fa868c8cafdee9c56 1737426 osbasic_0.0.9.orig.tar.gz
 1e6a604e073f3506adc1bc2055d8ddf0 1712 osbasic_0.0.9-0ubuntu1.diff.gz
