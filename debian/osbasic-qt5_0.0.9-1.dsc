Format: 1.0
Source: osbasic-qt5
Binary: osbasic-qt5
Architecture: all
Version: 0.0.9-1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 osbasic-qt5 deb contrib/editors optional arch=all
Checksums-Sha1:
 26857bfb9fc1b129fb212cacd69de5dab59ac05b 983 osbasic-qt5_0.0.9-1.tar.gz
Checksums-Sha256:
 cb1f7dfd6e03baca8ac49ab3411e41267f9c8400b027b7cb243279c1bf745f83 983 osbasic-qt5_0.0.9-1.tar.gz
Files:
 f9ef3c3aeb5c42616560f90164fc81f4 983 osbasic-qt5_0.0.9-1.tar.gz
