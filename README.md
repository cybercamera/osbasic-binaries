## Welcome

This repo holds the various binaries of the builds for osbasic. There are separate directories for each distro. In general, you would choose your distro then grab the *all* package and install that. There are some additional packages which can be used to bootstrap you into a qt4 or qt5 library environment. osbasic can work with either. Choose whichever matches your distros available library suite.

Any issues, rause a ticket on here.

Cheerio.
