Format: 1.0
Source: osbasic-qt4
Binary: osbasic-qt4
Architecture: all
Version: 0.0.9-1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 osbasic-qt4 deb contrib/editors optional arch=all
Checksums-Sha1:
 03798bf9e11adae2c849285c9ae65a49f052babe 983 osbasic-qt4_0.0.9-1.tar.gz
Checksums-Sha256:
 5ff2ba487370dc55f86d70ea2e5acb13694d312eaef205f56d18bfe4a77fa2b6 983 osbasic-qt4_0.0.9-1.tar.gz
Files:
 4dd019789d9341920d0ca314c81e5a60 983 osbasic-qt4_0.0.9-1.tar.gz
