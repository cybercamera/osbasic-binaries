Format: 1.0
Source: osbasic
Binary: osbasic
Architecture: all
Version: 0.0.9-1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9), gambas3-dev (>= 3.16), gambas3-dev (<< 3.99.0), gambas3-gb-args (>= 3.16), gambas3-gb-args (<< 3.99.0), gambas3-gb-image (>= 3.16), gambas3-gb-image (<< 3.99.0), gambas3-gb-form (>= 3.16), gambas3-gb-form (<< 3.99.0), gambas3-gb-eval-highlight (>= 3.16), gambas3-gb-eval-highlight (<< 3.99.0), gambas3-gb-util (>= 3.16), gambas3-gb-util (<< 3.99.0), gambas3-gb-form-editor (>= 3.16), gambas3-gb-form-editor (<< 3.99.0), gambas3-gb-term (>= 3.16), gambas3-gb-term (<< 3.99.0), gambas3-gb-form-terminal (>= 3.16), gambas3-gb-form-terminal (<< 3.99.0), gambas3-script
Package-List:
 osbasic deb contrib/editors optional arch=all
Checksums-Sha1:
 cd65441e67ae854e755d300b63a2bc2751a2a2dc 1737426 osbasic_0.0.9.orig.tar.gz
 d3da3b4f2a51b78eb8db0f4d004cf25e5561f48a 1707 osbasic_0.0.9-1.diff.gz
Checksums-Sha256:
 18fe3022518872dac81f1c0fd2828d433d3166f63d3bd1e8752233d74c418320 1737426 osbasic_0.0.9.orig.tar.gz
 5ec775a5a0ef7a4ffdadb5e82ef646583fa60e8046e2e560bda6be4e2030a944 1707 osbasic_0.0.9-1.diff.gz
Files:
 2189d5a00265398fa868c8cafdee9c56 1737426 osbasic_0.0.9.orig.tar.gz
 cd922fe728e883d078c08369b5f4dcc1 1707 osbasic_0.0.9-1.diff.gz
