Format: 1.0
Source: osbasic-qt4
Binary: osbasic-qt4
Architecture: all
Version: 0.0.9-0ubuntu1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 osbasic-qt4 deb contrib/editors optional arch=all
Checksums-Sha1:
 4d8468a5bd7cd5f2165b9ded412b642faf0f6764 987 osbasic-qt4_0.0.9-0ubuntu1.tar.gz
Checksums-Sha256:
 a01e2270e6b35382ec19732cb8dc3686f9635112984cbabb6f939c17d280e1b9 987 osbasic-qt4_0.0.9-0ubuntu1.tar.gz
Files:
 9d72e87e4f3918fffdcac1f0daf6eea8 987 osbasic-qt4_0.0.9-0ubuntu1.tar.gz
