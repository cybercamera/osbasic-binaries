Format: 1.0
Source: osbasic-qt5
Binary: osbasic-qt5
Architecture: all
Version: 0.0.9-0ubuntu1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 osbasic-qt5 deb contrib/editors optional arch=all
Checksums-Sha1:
 12d8dbb450d4ca26b5caa591d9fe3f7e009869ec 987 osbasic-qt5_0.0.9-0ubuntu1.tar.gz
Checksums-Sha256:
 e9b4b4fe0bb31e7736b4fce97ec0940b3af3c9bd1d2bee505badfe3bb0ed8baa 987 osbasic-qt5_0.0.9-0ubuntu1.tar.gz
Files:
 5242f760d1c92a7ce6e3d410edc23c9c 987 osbasic-qt5_0.0.9-0ubuntu1.tar.gz
